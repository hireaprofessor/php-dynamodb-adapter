<?php
namespace Apl;

use Apl\Helpers\ClassesHelper;
use Apl\Helpers\Str;
use Apl\TableDefinitions\TableDefinitionInterface;
use Aws\DynamoDb\DynamoDbClient as AWSDynamoDBClient;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;
use Aws\Exception\CredentialsException;
use Aws\Result;
use Apl\Exceptions\DynamoDbException as AwsDynamoDbException;
use Aws\Sdk;
use Apl\Helpers\ForwardsCalls;
use Exception;
use ReflectionClass;


/**
 * DynamoDB adapter extension
 *
 * @mixin AWSDynamoDBClient
 * @method useExample : self
 *
 * Class Client
 * @package Apl
 */
class DynamoDBClient
{
    use ForwardsCalls;

    // Properties

    /**
     * @var AWSDynamoDBClient
     */
    protected AWSDynamoDBClient $dynamoDbClient;

    /**
     * @var Marshaler
     */
    protected Marshaler $marshaler;

    /**
     * @var array
     */
    protected static array $tableDefinitions = [];

    /**
     * @var string
     */
    protected static string $selectedTable = '';

    // Constructor

    /**
     * Client constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->marshaler = new Marshaler();

        $s3Region = $options['s3_region'] ?? null;
        $s3Version = $options['s3_version'] ?? null;
        $s3Key = $options['s3_key'] ?? null;
        $s3Secret = $options['s3_secret'] ?? null;

        if (!$s3Region || $s3Version || $s3Key || $s3Secret) {
            throw new CredentialsException('Invalid AWS SDK Credentials!');
        }

        $awsConfig = [
            'region'   => $s3Region,
            'version'  => $s3Version,
            'credentials' => [
                'key'    => $s3Key,
                'secret' => $s3Secret,
            ]
        ];

        $this->dynamoDbClient = (new Sdk($awsConfig))->createDynamoDb();
    }

    // Methods

    /**
     * @param $method
     * @param $arguments
     * @return $this|mixed
     */
    public function __call($method, $arguments)
    {
        try {
            $tableName = Str::replaceFirst('use', '', $method);
            if (
                Str::startsWith($method, 'use') &&
                Str::camel($method)===$method &&
                $table = $this->getTableDefinitionForTableName($tableName)
            ) {
                $tableTtlField = $table::getTableTtlField();
                if ($arguments[$tableTtlField] ?? false) {
                    $table::setTtl(intval($arguments[$tableTtlField]));
                    $this->enableTimeToLiveForTableIfNotEnabled($table);
                }

                static::$selectedTable = $table::getTableName();
                return $this;
            }

            return $this->forwardCallTo($this->getDynamoDbClient(), $method, $arguments);
        } catch (Exception $ex) {
            return $this;
        }
    }

    /**
     * @return AWSDynamoDBClient
     */
    public function getDynamoDbClient(): AWSDynamoDBClient
    {
        return $this->dynamoDbClient;
    }

    /**
     * @param TableDefinitionInterface $tableDefinition
     */
    public function addTableDefinition(TableDefinitionInterface $tableDefinition): void
    {
        static::$tableDefinitions[$tableDefinition::getTableName()] = $tableDefinition;
    }

    /**
     * @param string $tableDefinitionName
     * @return TableDefinitionInterface|null
     */
    public function getTableDefinitionForTableName(string $tableDefinitionName): ?TableDefinitionInterface
    {
        return (static::$tableDefinitions[$tableDefinitionName] ?? null);
    }

    /**
     * @return array
     */
    public function getTableDefinitions(): ?array
    {
        return static::$tableDefinitions;
    }

    /**
     * @return string
     */
    public function getSelectedTable(): ?string
    {
        return static::$selectedTable ?? null;
    }

    /**
     * @return Marshaler
     */
    public function getMarshaler(): Marshaler
    {
        return $this->marshaler;
    }

    /**
     * @param bool $returnExistentTables
     * @return Result|null
     */
    public function createTablesIfNotExists(bool $returnExistentTables = false) :? Result
    {
        $tableNames = array_keys($this->getTableDefinitions());

        foreach ($tableNames as $tableName) {
            $tableDefinition = $this->getTableDefinitionForTableName($tableName);
            if ($tableDefinition) {
                try {
                    $this->describeTable([
                        'TableName' => $tableName
                    ]);
                } catch (DynamoDbException $eDescribe) {
                    try {
                        $this->createTable(
                            $tableDefinition->getTableStructureForCreation()
                        );
                    } catch (DynamoDbException $eCreate) {
                        continue;
                    }
                }

                if ($tableDefinition::getTtl()) {
                    $this->enableTimeToLiveForTableIfNotEnabled($tableDefinition);
                }
            }
        }

        if ($returnExistentTables) {
            return $this->listTables([]);
        }
        return null;
    }

    /**
     * @param TableDefinitionInterface $tableDefinition
     * @return bool
     */
    public function enableTimeToLiveForTableIfNotEnabled(TableDefinitionInterface $tableDefinition) : bool
    {
        $tableName = $tableDefinition::getTableName();

        try {
            $result = $this->describeTimeToLive([
                'TableName' => $tableName
            ]);

            $timeToLiveDescription = $result->get('TimeToLiveDescription');

            if ($timeToLiveDescription['TimeToLiveStatus'] == 'DISABLED') {
                 $this->updateTimeToLive([
                     'TableName' => $tableName,
                     'TimeToLiveSpecification' => [
                         'Enabled' => true,
                         'AttributeName' => 'ttl',
                     ],
                 ]);
            }
            return true;
        } catch (DynamoDbException $eTtl) {
            return false;
        }
    }

    /**
     * @param string $folder
     */
    public function loadTableDefinitionsFromFolder(string $folder)
    {
        if (!is_dir($folder)) {
            return;
        }

        $handlerClasses = ClassesHelper::getClassesFromPath(
            $folder,
            function($class) {
                if (class_exists($class)) {
                    $tableDefinitionReflection = new ReflectionClass($class);
                    if (
                        $tableDefinitionReflection->isInstantiable() &&
                        $tableDefinitionReflection->implementsInterface(TableDefinitionInterface::class)
                    ) {
                        return true;
                    }
                }
                return false;
            }
        );

        foreach ($handlerClasses as $class){
            $this->addTableDefinition(new $class);
        }
    }

    /**
     *
     * @param TableDefinitionInterface;
     * @throw AwsDynamoDbException;
     * @return string;
     */
    public function resolveTableName(TableDefinitionInterface $table = null): string
    {
        $tableName = is_null($table) ? static::$selectedTable : $table::getTableName();
        if (empty($tableName)) {
            throw new AwsDynamoDbException('No DynamoDB Table Selected!');
        }
        return $tableName;
    }

    /**
     * See: @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.PHP.03.html
     * @return Result
     */
    public function create(array $data, TableDefinitionInterface $table = null) :? Result
    {
        $tableName = $this->resolveTableName($table);

        $tableDefinition = $this->getTableDefinitionForTableName($tableName);

        $ttlField = $tableDefinition::getTableTtlField();

        if ($data[$ttlField] ?? false) {
            $tableDefinition::setTtl(intval($data[$ttlField]));
            $this->enableTimeToLiveForTableIfNotEnabled($tableDefinition);
        }

        $params = [
            'TableName' => $tableName,
            'Item' => $this->getMarshaler()->marshalItem($data)
        ];

        try {
            $result = $this->putItem($params);
            return $result;
        } catch (DynamoDbException $e) {
            return null;
        }
    }

    /**
     * See: @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.PHP.03.html
     * @return Result
     */
    public function read(array $data, TableDefinitionInterface $table = null) :? Result
    {
        $tableName = $this->resolveTableName($table);

        $params = [
            'TableName' => $tableName,
            'Key' => $this->getMarshaler()->marshalItem($data)
        ];

        try {
            $result = $this->getItem($params);
            return $result;
        } catch (DynamoDbException $e) {
            return null;
        }
    }

    /**
     * See: @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.PHP.03.html
     * @return Result
     */
    public function update(array $key, array $data, TableDefinitionInterface $table = null) :? Result
    {
        // @ToDo Implement the update
    }

    /**
     * See: @link https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.PHP.03.html
     * @return Result
     */
    public function delete(array $data, TableDefinitionInterface $table = null) :? Result
    {
        $tableName = $this->resolveTableName($table);

        $params = [
            'TableName' => $tableName,
            'Key' => $this->getMarshaler()->marshalItem($data)
        ];

        try {
            $result = $this->deleteItem($params);
            return $result;
        } catch (DynamoDbException $e) {
            return null;
        }
    }
}
