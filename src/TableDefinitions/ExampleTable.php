<?php
namespace Apl\TableDefinitions;

class ExampleTable extends TableDefinition
{
    /**
     * @var int
     */
    protected static int $ttl = 300;

    /**
     * @return string
     */
    public static function getTableName() : string
    {
        return "Example";
    }

    /**
     * @return array
     */
    public static function getAttributeDefinition() : array
    {
        return [
            [
                'AttributeName' => 'environment',
                'AttributeType' => 'S'
            ],
            [
                'AttributeName' => 'key',
                'AttributeType' => 'S'
            ],
            [
                'AttributeName' => 'ttl',
                'AttributeType' => 'N'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function getKeySchema() : array
    {
        return [
            [
                'AttributeName' => 'environment',
                'KeyType' => 'HASH'
            ],
            [
                'AttributeName' => 'key',
                'KeyType' => 'RANGE'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function getProvisionedThroughput() : array
    {
        return [
            'ReadCapacityUnits' => 10,
            'WriteCapacityUnits' => 10
        ];
    }
}
