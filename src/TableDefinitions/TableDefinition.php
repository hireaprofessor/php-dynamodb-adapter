<?php
namespace Apl\TableDefinitions;

abstract class TableDefinition implements TableDefinitionInterface
{
    /**
     * @var int
     */
    protected static int $ttl;

    /**
     * @var string
     */
    protected static string $ttlField = 'ttl';

    /**
     * @return array
     */
    public function getTableStructureForCreation() : array
    {
        return [
            'TableName' => static::getTableName(),
            'KeySchema' => static::getKeySchema(),
            'AttributeDefinitions' => static::getAttributeDefinition(),
            'ProvisionedThroughput' => static::getProvisionedThroughput()
        ];
    }

    /**
     * @return int
     */
    public static function getTtl () : int
    {
        return static::$ttl;
    }

    /**
     * @param int $ttl
     */
    public static function setTtl (int $ttl)
    {
        static::$ttl = $ttl;
    }

    /**
     * @return string
     */
    public static function getTableTtlField (): string
    {
        return static::$ttlField;
    }
}