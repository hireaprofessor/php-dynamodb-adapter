<?php
namespace Apl\TableDefinitions;

interface TableDefinitionInterface
{
    public static function getTableName() : string;
    public static function getAttributeDefinition() : array;
    public static function getKeySchema() : array;
    public static function getProvisionedThroughput() : array;
    public static function getTtl() : int;
    public static function setTtl(int $ttl);
    public static function getTableTtlField() : string;
    public function getTableStructureForCreation() : array;
}
