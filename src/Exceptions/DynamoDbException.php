<?php
namespace Apl\Exceptions;

use Aws\DynamoDb\Exception\DynamoDbException as AwsDynamoDbException;

class DynamoDbException extends AwsDynamoDbException
{
}
