<?php
namespace Apl\Helpers;

use Closure;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class ClassesHelper
 * @package Apl\Helpers
 */
class ClassesHelper
{
    /**
     * @param string $startingPath
     * @param Closure|null $filter
     * @return array
     */
    public static function getClassesFromPath(string $startingPath, Closure $filter = null) : array
    {
        $results = [];

        $startingDir = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($startingPath)
        );

        foreach ($startingDir as $item) {
            if (!$item->isFile()) {
                continue;
            }

            $path = $item->getRealPath();

            if (pathinfo($path, PATHINFO_EXTENSION) !== 'php') {
                continue;
            }

            $classes = self::findClasses($path);
            foreach ($classes as $class) {
                $results[] = $class;
            }
        }

        if (count($results) && $filter && is_callable($filter)) {
            $results = array_values(array_filter($results, $filter));
        }

        return $results;
    }

    /**
     * @param $path
     * @return array
     */
    private static function findClasses($path) : array
    {
        $contents = file_get_contents($path);
        $tokens   = token_get_all($contents);

        $results = [];

        $namespace = '';
        for ($i = 0, $max = count($tokens); $i < $max; $i++) {
            $token = $tokens[$i];

            if (is_string($token)) {
                continue;
            }

            $class = '';

            switch ($token[0]) {
                case T_NAMESPACE:
                    $namespace = '';
                    // If there is a namespace, extract it
                    while (($t = $tokens[++$i]) && is_array($t)) {
                        if (in_array($t[0], array(T_STRING, T_NS_SEPARATOR))) {
                            $namespace .= $t[1];
                        }
                    }
                    $namespace .= '\\';
                    break;
                case T_CLASS:
                case T_INTERFACE:
                case T_TRAIT:
                    // Find the classname
                    while (($t = $tokens[++$i]) && is_array($t)) {
                        if (T_STRING === $t[0]) {
                            $class .= $t[1];
                        } elseif ($class !== '' && T_WHITESPACE == $t[0]) {
                            break;
                        }
                    }

                    $class = ltrim($namespace.$class, '\\');
                    if (class_exists($class)) {
                        $results[] = $class;
                    }

                    break;
                default:
                    break;
            }
        }

        return $results;
    }
}
