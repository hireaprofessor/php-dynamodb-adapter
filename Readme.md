# Example usage of DynamoDB adapter implementation:

    use \Apl\DynamoDBClient;

    $dynamoDb = new DynamoDBClient([
        's3_region' => 'your s3 region',
        's3_version' => 'your s3 version',
        's3_key' => 'your s3 key',
        's3_secret' => 'your s3 secret',
    ]);

    $dynamoDb->loadTableDefinitionsFromFolder('/path/to/your/table/definition/classes');

    $dynamoDb->createTablesIfNotExists();

    // create an item
    $result = $dynamoDb->useExample()->create([
        'environment' => 'production',
        'key' => 'lock',
        'value' => 'locked',
        'ttl' => 300
    ]);

    // read back an item
    $result = $dynamoDb->useExample()->read([
        'environment' => 'production',
        'key' => 'lock'
    ]);
    
    // delete an item
    $result = $dynamoDb->useExample()->delete([
        'environment' => 'production',
        'key' => 'lock'
    ]);

- The `useExample()` method will select the current table to work on. 
This method is a virtual method, where the word `Example` reflects the string returned by `ExampleTable::getTableName()`. 
- To create a new table, just create an other implementation of the `TableDefinitionInterface` by extending `TableDefinition` abstract class
- To enable TTL, add `['ttl'=>600]` as option to the `useExample()` method. This will add a TTL of 600 seconds to the item.
For more info, visit https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.PHP.03.html

@todo: Implement update method